<?php

declare(strict_types=1);

namespace Drupal\sitetree_domain\DomainList;

use Drupal\Core\Entity\EntityTypeManagerInterface;

final class DomainListService {

  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {}

  public  function getDomainList(): DomainList {
    /** @var \Drupal\domain\DomainStorageInterface $domainStorage */
    $domainStorage = $this->entityTypeManager->getStorage('domain');
    $defaultDomain = $domainStorage->loadDefaultDomain();
    $allDomains = $domainStorage->loadMultipleSorted();
    return new DomainList($defaultDomain, $allDomains);
  }

}
