<?php

declare(strict_types=1);

namespace Drupal\sitetree_domain\DomainList;

use Drupal\domain\DomainInterface;

final class DomainList {

  public readonly array $allDomainsById;

  /**
   * @param \Drupal\domain\DomainInterface[] $allDomainsById
   */
  public function __construct(
    public readonly ?DomainInterface $defaultDomain,
           array                     $allDomainsById,
  ) {
    // Guarantee that DefaultDomain is first.
    if ($this->defaultDomain) {
      $allDomainsById = [
        $this->defaultDomain->id()=> $this->defaultDomain
      ] + $allDomainsById;
    }
    $this->allDomainsById = $allDomainsById;
  }

  public function getDomain(string $domainId): DomainInterface {
    return $this->allDomainsById[$domainId] ?? throw new \UnexpectedValueException();
  }

}
