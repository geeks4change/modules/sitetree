<?php

declare(strict_types=1);

namespace Drupal\sitetree_domain;

use Drupal\domain\DomainNegotiatorInterface;
use Drupal\sitetree\SiteTree\SiteTreeHandlerInterface;
use Drupal\sitetree\SiteTree\SiteTreeMatcherInterface;
use Drupal\sitetree_domain\EffectiveUrlResult\EffectiveUrlResultAssembler;
use Symfony\Component\HttpFoundation\RequestStack;

final class DomainSiteTreeMatcher implements SiteTreeMatcherInterface {

  public function __construct(
    protected DomainNegotiatorInterface $domainNegotiator,
    protected EffectiveUrlResultAssembler $effectiveUrlResultAssembler,
    protected RequestStack                $requestStack,
  ) {}

  public function getSiteTreeHandler(): SiteTreeHandlerInterface {
    // @todo Remove this factory class.
    return new DomainSiteTreeHandler(
      $this->domainNegotiator,
      $this->effectiveUrlResultAssembler,
      $this->requestStack,
    );
  }

}
