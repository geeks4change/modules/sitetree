<?php

declare(strict_types=1);

namespace Drupal\sitetree_domain\EffectiveUrlResult;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Url;
use Drupal\sitetree\LinkTrail\MenuLinkTrailMap;
use Drupal\sitetree\MenuLinkUtility\MenuLinkTrailMatcher;
use Drupal\sitetree\Utility\DomainTool;
use Drupal\sitetree\Utility\FrontPageMatcher;
use Drupal\sitetree_domain\SiteMenuNames\DomainSiteMenuNamesService;

final class EffectiveUrlResultAssembler {

  public function __construct(
    protected MenuLinkTrailMatcher       $menuLinkTrailMatcher,
    protected DomainSiteMenuNamesService $domainSiteMenuNamesService,
    protected FrontPageMatcher           $frontPageMatcher,
  ) {}

  /**
   * Assembles the effective URL result for all domain sites.
   *
   * @todo Cache this by URL.
   */
  public function getEffectiveResult(Url $url): EffectiveUrlResult {
    $allMenuLinkTrails = $this->menuLinkTrailMatcher->getAllMenuLinkTrails($url);
    $domainMenuNames = $this->domainSiteMenuNamesService->getDomainSiteMenuNames();
    $domainList = $domainMenuNames->domains;


    // Collect relevant menuLinkTrails.
    $exclusiveMatchExists = FALSE;
    // If a site menu claims exclusive access, collect only that trail.
    foreach ($domainList->allDomainsById as $domainId => $domain) {
      if ($exclusiveMenuNames = $domainMenuNames->exclusive->getMenuNames($domainId)) {
        $exclusiveTrail = $allMenuLinkTrails->getFirstMenuLinkTrailForMenus(...$exclusiveMenuNames);
        if ($exclusiveTrail) {
          $exclusiveMenuLinkTrailsBySite = [$domainId => $exclusiveTrail];
        }
      }
    }
    if (isset($exclusiveMenuLinkTrailsBySite)) {
      $menuLinkTrailsBySite = $exclusiveMenuLinkTrailsBySite;
      $exclusiveMatchExists = TRUE;
    }
    else {
      // Otherwise, collect all trails.
      $menuLinkTrailsBySite = [];
      foreach ($domainList->allDomainsById as $domainId => $domain) {
        $accessMenuNames = $domainMenuNames->all->getMenuNames($domainId);
        $accessTrail = $allMenuLinkTrails->getFirstMenuLinkTrailForMenus(...$accessMenuNames);
        $menuLinkTrailsBySite[$domainId] = $accessTrail;
      }
    }


    // Find the canonical base url.
    $explicitCanonicalBaseUrl = NULL;
    foreach ($domainList->allDomainsById as $domainId => $domain) {
      if ($canonicalMenuNames = $domainMenuNames->canonical->getMenuNames($domainId)) {
        $canonicalTrail = $allMenuLinkTrails->getFirstMenuLinkTrailForMenus(...$canonicalMenuNames);
        if ($canonicalTrail) {
          $explicitCanonicalBaseUrl = DomainTool::getBaseUrl($domain);
        }
      }
    }


    // Preset the outbound base url: Canonical if exists, otherwise first.
    if ($explicitCanonicalBaseUrl) {
      $outboundBaseUrlIfNoAccess = $explicitCanonicalBaseUrl;
    }
    else {
      $siteId = $this->getFirstNonNullItemKey($menuLinkTrailsBySite);
      if ($siteId) {
        $outboundBaseUrlIfNoAccess = DomainTool::getBaseUrl($domainList->getDomain($siteId));
      }
      elseif ($domainList->defaultDomain) {
        $outboundBaseUrlIfNoAccess = DomainTool::getBaseUrl($domainList->defaultDomain);
      }
      else {
        $outboundBaseUrlIfNoAccess = NULL;
      }
    }


    // Use outboundBaseUrl as base for canonicalUrl to always have one.
    // @todo Consider making this configurable once needed.
    $canonicalUrl = !isset($outboundBaseUrlIfNoAccess) ? NULL :
      $this->withBaseUrl($url, $outboundBaseUrlIfNoAccess)
    ;


    $cacheability = (new CacheableMetadata())
      ->addCacheableDependency($domainMenuNames->getCacheability())
      ->addCacheableDependency($allMenuLinkTrails->getCacheability());

    return new EffectiveUrlResult(
      new MenuLinkTrailMap($menuLinkTrailsBySite, $cacheability),
      $exclusiveMatchExists,
      $outboundBaseUrlIfNoAccess,
      $canonicalUrl?->toString(),
    );
  }

  private function getFirstNonNullItemKey(array $items): ?string {
    foreach ($items as $key => $item) {
      if (isset($item)) {
        return $key;
      }
    }
    return NULL;
  }

  public function withBaseUrl(Url $url, string $baseUrl): Url {
    $isFrontPage = $this->frontPageMatcher->isFrontPage($url)->value();
    $newUrl = $isFrontPage ?
      Url::fromUserInput('/') : clone $url;
    return $newUrl
      ->setOption('base_url', $baseUrl)
      ->setAbsolute();
  }

}
