<?php

declare(strict_types=1);

namespace Drupal\sitetree_domain\EffectiveUrlResult;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\sitetree\LinkTrail\MenuLinkTrail;
use Drupal\sitetree\LinkTrail\MenuLinkTrailMap;

/**
 * Implements the SiteTree rules (see Readme).
 *
 * @see \Drupal\sitetree_domain\EffectiveUrlResult\EffectiveUrlResultAssembler::getEffectiveResult
 *
 * This intentionally does NOT implement front page path, as this is local to
 * every domain, and generation needs MenuLinkManager, which is likely to
 * cause circular dependencies, and is not necessary here.
 * @todo Consider changing this, adding getFrontPageUrl($siteId).
 * @see \Drupal\sitetree_domain\ConfigOverride\SiteTreeDomainFrontPageConfigOverride::generateFrontPagePath
 */
final class EffectiveUrlResult {

  public function __construct(
    protected readonly MenuLinkTrailMap $menuLinkTrailsBySite,
    protected readonly bool             $exclusiveMatchExists,
    protected readonly ?string          $outboundBaseUrlIfNoAccess,
    protected readonly ?string          $canonicalUrl,
  ) {}

  public function getCacheability(): CacheableDependencyInterface {
    // No additional cacheability needed.
    return $this->menuLinkTrailsBySite->getCacheability();
  }

  public function hasAccess(string $siteId, bool $onDefaultSite): bool {
    $accessTrail = $this->menuLinkTrailsBySite->getMenuLinkTrail($siteId);
    return !empty($accessTrail)
      || (
        $onDefaultSite
        && !$this->exclusiveMatchExists
      );
  }

  public function getOutboundBaseUrl(string $siteId, bool $onDefaultSite): ?string {
    $hasAccess = $this->hasAccess($siteId, $onDefaultSite);
    if (!$hasAccess) {
      return $this->outboundBaseUrlIfNoAccess;
    }
    else {
      return NULL;
    }
  }

  public function getMenuLinkTrailBySite(string $siteId): ?MenuLinkTrail {
    return $this->menuLinkTrailsBySite->getMenuLinkTrail($siteId);
  }

  public function getCanonicalUrl(): ?string {
    return $this->canonicalUrl;
  }

}
