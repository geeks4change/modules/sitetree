<?php

declare(strict_types=1);

namespace Drupal\sitetree_domain\Redirect;

use Drupal\Core\Routing\RequestContext;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Url;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\sitetree\Utility\BaseUrlTrait;
use Drupal\sitetree_domain\EffectiveUrlResult\EffectiveUrlResultAssembler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

final class DomainSiteTreeRedirectRequestSubscriber implements EventSubscriberInterface {

  use BaseUrlTrait;

  public function __construct(
    protected EffectiveUrlResultAssembler $effectiveUrlResultAssembler,
    protected UrlGeneratorInterface       $urlGeneratorNonBubbling,
    protected DomainNegotiatorInterface   $domainNegotiator,
  ) {}

  public static function getSubscribedEvents() {
    // This needs to run before RouterListener::onKernelRequest(), which has
    // a priority of 32. Otherwise, that aborts the request if no matching
    // route is found.
    $events[KernelEvents::REQUEST][] = ['onRequestRedirectIfNeeded', 33];
    return $events;
  }

  public function onRequestRedirectIfNeeded(RequestEvent $event) {
    // Clone to prevent modification.
    $request = clone $event->getRequest();
    try {
      $url = Url::createFromRequest($request);
    } catch (ResourceNotFoundException) {
      return;
    }
    $effectiveResult = $this->effectiveUrlResultAssembler->getEffectiveResult($url);

    $currentDomain = $this->domainNegotiator->getActiveDomain()
      ?? $this->domainNegotiator->getActiveDomain(TRUE);
    if (!$currentDomain) {
      return;
    }
    // Get currentBaseUrl from request, not currentDomain, because we CAN be
    // "on default domain" with base url DIFFERENT from that of current
    // domain, because default domain is fallback. In which case we want to
    // redirect.
    // @link https://git.drupalcode.org/project/redirect/-/blob/8.x-1.x/src/EventSubscriber/RedirectRequestSubscriber.php
    // @link https://git.drupalcode.org/project/redirect/-/blob/8.x-1.x/src/RedirectChecker.php
    // Technically we'd have to add the redirect cacheability here. Factually
    // this is done when we check access in route access controller.

    // UrlGenerator needs this to get baseUrl.
    $requestContext = (new RequestContext())->fromRequest($request);
    $this->urlGeneratorNonBubbling->setContext($requestContext);
    // May be different from currentDomain base url.
    $currentBaseUrl = $this->getBaseUrl();
    $redirectBaseUrl = $effectiveResult->getOutboundBaseUrl($currentDomain->id(), $currentDomain->isDefault());
    $redirectCacheability = $effectiveResult->getCacheability();

    if ($redirectBaseUrl && $redirectBaseUrl !== $currentBaseUrl) {
      // @see \Drupal\Core\Routing\UrlGenerator::generateFromRoute
      $url->setOptions(['base_url' => $redirectBaseUrl] + $url->getOptions());
      $headers = ['X-Redirect-Handler' => 'SiteTreeDomain'];
      // @fixme Remove processOutbound recursion.
      $urlAsString = $url->setAbsolute()->toString();
      // Signal temporary redirect 302.
      $response = new TrustedRedirectResponse($urlAsString, 302, $headers);
      $response->addCacheableDependency($redirectCacheability);
      $event->setResponse($response);
    }
  }

}
