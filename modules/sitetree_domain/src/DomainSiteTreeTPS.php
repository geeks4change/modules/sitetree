<?php

namespace Drupal\sitetree_domain;

use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\domain\DomainInterface;
use Drupal\sitetree_domain\SiteMenuNames\DomainMenuMode;
use Drupal\system\Entity\Menu;
use Drupal\system\MenuInterface;

final class DomainSiteTreeTPS {

  public static function staticHookFormDomainFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $formObject = $form_state->getFormObject();
    assert($formObject instanceof EntityFormInterface);
    $domain = $formObject->getEntity();
    assert($domain instanceof DomainInterface);

    // Add entity builder in any case, maybe default changes.
    $form['#entity_builders'][] = [static::class, 'entityBuilder'];
    //$form['#validate'][] = [static::class, 'validateForm'];
    $form['#validate'][] = static::class . '::validateForm';

    if ($domain->isDefault()) {
      return;
    }

    $form['third_party_settings']['#tree'] = TRUE;
    $tpsForm =& $form['third_party_settings']['sitetree_domain'];

    $tpsForm['#type'] = 'fieldset';
    $tpsForm['#title'] = t('SiteTree settings');

    $menusByName = \Drupal::entityTypeManager()->getStorage('menu')->loadMultiple();
    uasort($menusByName, [Menu::class, 'sort']);
    $menuOptions = array_map(fn(MenuInterface $menu) => $menu->label(), $menusByName);

    // @todo Replace with ordered_list once this is D10 ready.
    //   https://www.drupal.org/project/ordered_list/issues/3288915
    $menuModes = $domain->getThirdPartySetting('sitetree_domain', 'menus', []);
    $defaultPrimary = array_key_first($menuModes);

    $tpsForm['menus']['modes']['#type'] = 'fieldset';
    $tpsForm['menus']['modes']['#title'] = t('Menu Modes');
    $tpsForm['menus']['modes']['#description_display'] = 'before';
    $tpsForm['menus']['modes']['#description'] = t('Only items of active menus are visible on this domain..');
    foreach ($menuOptions as $menuName => $menuLabel) {
      $tpsForm['menus']['modes'][$menuName] = [
        '#type' => 'select',
        '#title' => $menuLabel,
        '#default_value' => $menuModes[$menuName] ?? DomainMenuMode::None->value,
        '#options' => DomainMenuMode::options(),
        '#required' => FALSE,
      ];
    }

    $tpsForm['menus']['primary'] = [
      '#type' => 'select',
      '#title' => t('Primary menu'),
      '#description' => t('The first item of the primary menu will become the front page.'),
      '#description_display' => 'before',
      '#multiple' => FALSE,
      '#default_value' => $defaultPrimary,
      '#options' => $menuOptions,
      '#required' => FALSE,
      '#empty_option' => t('- Select -'),
    ];
  }

  public static function validateForm(&$form, FormStateInterface $formState) {
    $settings = $formState->getValue(['third_party_settings', 'sitetree_domain']);
    $primary = $settings['menus']['primary'];
    if ($primary && $settings['menus']['modes'][$primary] === DomainMenuMode::None->value) {
      $formState->setError($form['third_party_settings']['sitetree_domain']['menus']['primary'], t('Primary menu must not be inactive'));
    }
  }

  public static function entityBuilder(string $entity_type, DomainInterface $domain, &$form, FormStateInterface $formState) {
    // EntityForm already copied stuff over, so only massage needed.
    $settings = $domain->getThirdPartySettings('sitetree_domain');

    // Only if non-default domain.
    if ($settings) {
      // Massage menu settings, so primary menu is first.
      $menuModes = $settings['menus']['modes'];
      $primaryMenuName = $settings['menus']['primary'];
      // Make primary menu first.
      if ($primaryMenuName) {
        $primaryMenuMode = $menuModes[$primaryMenuName];
        unset($menuModes[$primaryMenuMode]);
        $menuModes = [$primaryMenuName => $primaryMenuMode] + $menuModes;
      }
      // Filter by mode.
      $menuModes = array_filter($menuModes, fn(string $mode) => $mode !== DomainMenuMode::None->value);
      $settings['menus'] = $menuModes;
    }

    // There is no ::setThirdPartySettings, sigh.
    foreach ($settings as $key => $setting) {
      if ($setting) {
        $domain->setThirdPartySetting('sitetree_domain', $key, $setting);
      }
      else {
        $domain->unsetThirdPartySetting('sitetree_domain', $key);
      }
    }
  }

  public static function getMenuNames(DomainInterface $domain): array {
    return self::getMenuNamesFiltered($domain);
  }

  public static function getExclusiveMenuNames(DomainInterface $domain): array {
    return self::getMenuNamesFiltered($domain, DomainMenuMode::Exclusive);
  }

  public static function getCanonicalMenuNames(DomainInterface $domain): array {
    return self::getMenuNamesFiltered($domain, DomainMenuMode::Canonical);
  }

  public static function getMenuNamesFiltered(DomainInterface $domain, DomainMenuMode $filterMode = NULL): array {
    $menuModes = $domain->getThirdPartySetting('sitetree_domain', 'menus', []);
    if ($filterMode) {
      $menuModes = array_filter($menuModes, fn(string $mode) => $mode === $filterMode->value);
    }
    return array_keys($menuModes);
  }

}
