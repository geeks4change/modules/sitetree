<?php

declare(strict_types=1);

namespace Drupal\sitetree_domain;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\sitetree\LinkTrail\MenuLinkTrail;
use Drupal\sitetree\SiteTree\SiteTreeHandlerInterface;
use Drupal\sitetree\Utility\DomainTool;
use Drupal\sitetree_domain\EffectiveUrlResult\EffectiveUrlResultAssembler;
use Symfony\Component\HttpFoundation\RequestStack;

final class DomainSiteTreeHandler implements SiteTreeHandlerInterface {

  public function __construct(
    protected DomainNegotiatorInterface   $domainNegotiator,
    protected EffectiveUrlResultAssembler $effectiveUrlResultAssembler,
    protected RequestStack                $requestStack,
  ) {}

  public function getAccess(Url $url, AccountInterface $account): AccessResultInterface {
    $effectiveResult = $this->effectiveUrlResultAssembler->getEffectiveResult($url);
    $currentDomain = $this->domainNegotiator->getActiveDomain();

    if ($currentDomain) {
      $hasAccess = $effectiveResult->hasAccess($currentDomain->id(), $currentDomain->isDefault());
    }
    else {
      // If any domain is configured as default domain, any unrecognized url
      // maps to this default domain. So if no currentDomain is set, this means
      // that no domains are configured. In this case, let everything pass.
      $hasAccess = TRUE;
    }

    if ($hasAccess) {
      // @todo Switch to CacheableBool for clarity.
      return AccessResult::allowed()->addCacheableDependency($effectiveResult);
    }
    else {
      return AccessResult::forbidden()
        ->addCacheableDependency($effectiveResult->getCacheability());
    }
  }

  public function processOutbound(string &$path, array &$options, ?string &$baseUrl, BubbleableMetadata $bubbleable_metadata): void {
    $currentDomain = $this->domainNegotiator->getActiveDomain();
    if (!$currentDomain) {
      return;
    }

    $url = Url::fromUri("internal:/$path")->setOptions($options);
    $effectiveResult = $this->effectiveUrlResultAssembler->getEffectiveResult($url);
    $outboundBaseUrl = $effectiveResult->getOutboundBaseUrl($currentDomain->id(), $currentDomain->isDefault());
    if (
      $outboundBaseUrl
      && $outboundBaseUrl !== DomainTool::getBaseUrl($currentDomain)
    ) {
      $baseUrl = $outboundBaseUrl;
    }
  }

  public function getCompleteLinkTrail(Url $url): ?MenuLinkTrail {
    $currentDomain = $this->domainNegotiator->getActiveDomain();
    if (!$currentDomain) {
      return NULL;
    }
    $effectiveResult = $this->effectiveUrlResultAssembler->getEffectiveResult($url);
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $menuLinkTrail = $effectiveResult->getMenuLinkTrailBySite($currentDomain->id());
    return $menuLinkTrail;
  }

  public function getCacheability(): CacheableDependencyInterface {
    // @todo Get this from effectiveResult (or add list cacheability to empty link trail).
    return (new CacheableMetadata())
      ->addCacheTags(['domain_list'])
      ->addCacheContexts(['url.site']);
  }

}
