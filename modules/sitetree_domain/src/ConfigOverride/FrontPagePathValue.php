<?php

declare(strict_types=1);

namespace Drupal\sitetree_domain\ConfigOverride;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;

final class FrontPagePathValue {

  public function __construct(
    public readonly ?string $path,
    public readonly CacheableDependencyInterface $cacheability,
  ) {}

  public function withCacheability(CacheableDependencyInterface $cacheability): static {
    $newCacheability = (new CacheableMetadata())
      ->addCacheableDependency($this->cacheability)
      ->addCacheableDependency($cacheability);
    return new static($this->path, $newCacheability);
  }

}
