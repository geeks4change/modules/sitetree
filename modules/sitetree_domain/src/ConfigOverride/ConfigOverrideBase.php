<?php

namespace Drupal\sitetree_domain\ConfigOverride;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\Cache\Context\ContextCacheKeys;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class ConfigOverrideBase implements ConfigFactoryOverrideInterface {

  public function __construct(
    protected RequestStack $requestStack,
    protected CacheContextsManager $cacheContextsManager,
  ) {}

  protected $recursion = 0;

  public function loadOverrides($names) {
    $overrides = $this->getOverrides($names);
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $filteredOverrides = array_intersect_key($overrides, array_flip($names));
    return $filteredOverrides;
  }

  public function getCacheSuffix() {
    return implode(':', $this->getCacheKeyInfo()->getKeys());
  }

  protected function getCacheKeyInfo(): ContextCacheKeys {
    // Consider adding recursionProtection here too.
    $cacheContextNames = $this->getAllItemsCacheability()->getCacheContexts();
    $keyInfo = $this->cacheContextsManager->convertTokensToKeys($cacheContextNames);
    return $keyInfo;
  }

  public function getCacheableMetadata($name) {
    $cacheabilityBuilder = new CacheableMetadata();
    $maybeSingleItemCacheability = $this->getSingleItemCacheability($name);
    if ($maybeSingleItemCacheability) {
      $cacheabilityBuilder->addCacheableDependency($maybeSingleItemCacheability);
      $cacheabilityBuilder->addCacheableDependency($this->getAllItemsCacheability());
    }
    return $cacheabilityBuilder;
  }

  /**
   * Get cacheability (only contexts used) to compute cache keys.
   * @return \Drupal\Core\Cache\CacheableDependencyInterface
   */
  abstract protected function getAllItemsCacheability(): CacheableDependencyInterface;

  /**
   * Get single item cacheability.
   *
   * If non-null, allItemCacheability is added.
   */
  abstract protected function getSingleItemCacheability(string $name): ?CacheableDependencyInterface;

  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

  abstract protected function getOverrides(array $names): array;

}
