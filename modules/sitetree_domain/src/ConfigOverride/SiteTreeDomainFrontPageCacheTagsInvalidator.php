<?php

declare(strict_types=1);

namespace Drupal\sitetree_domain\ConfigOverride;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\sitetree\MenuLinkUtility\MenuCacheability;

/**
 * Invalidate route_match when our frontPage override changes.
 *
 * Drupal still has a lot of manual invalidation, sigh.
 * Core manually invalidates frontpage when config:system.site is saved.
 * Invalidate frontPage when menus change.
 *
 * @see \Drupal\system\EventSubscriber\ConfigCacheTag::onSave
 */
final class SiteTreeDomainFrontPageCacheTagsInvalidator implements CacheTagsInvalidatorInterface {


  public function __construct(
    protected CacheTagsInvalidatorInterface $cacheTagsInvalidator,
  ) {}

  public function invalidateTags(array $tags) {
    if (array_filter($tags, fn(string $tag) => str_starts_with($tag, MenuCacheability::getPrefix()))) {
      $this->cacheTagsInvalidator->invalidateTags(['route_match']);
    }
  }

}
