<?php

namespace Drupal\sitetree_domain\ConfigOverride;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\sitetree\MenuLinkUtility\MenuCacheability;
use Drupal\sitetree\MenuLinkUtility\MenuFirstLinkFetcher;
use Drupal\sitetree_domain\DomainSiteTreeTPS;
use Symfony\Component\HttpFoundation\RequestStack;

class SiteTreeDomainFrontPageConfigOverride extends ConfigOverrideBase {

  protected \WeakMap $frontPagePathPerRequest;

  protected MenuFirstLinkFetcher $menuFirstLinkFetcher;

  protected DomainNegotiatorInterface $domainNegotiator;

  /** @noinspection PhpMissingParentConstructorInspection */
  public function __construct(
    protected RequestStack $requestStack,
    protected CacheContextsManager $cacheContextsManager,
  ) {
    $this->frontPagePathPerRequest = new \WeakMap();
  }

  /**
   * Get service without circular dependency hell.
   *
   * sitetree_domain.config_override -> domain.negotiator -> entity_type.manager -> string_translation -> string_translator.locale.lookup -> language_request_subscriber -> language_manager -> config.factory -> sitetree_domain.config_override
   */
  public function getDomainNegotiator(): DomainNegotiatorInterface {
    return $this->domainNegotiator ??
      ($this->domainNegotiator = \Drupal::service('domain.negotiator'));
  }

  /**
   * Get service without circular dependency hell.
   *
   * sitetree.config_override -> sitetree.utility.menu_first_link_fetcher -> menu.link_tree -> route_provider -> path_processor_manager -> path_processor_front -> config.factory
   *
   * @todo Consider using MenuLinkStorage in MenuFirstLinkFetcher.
   */
  public function getMenuFirstLinkFetcher(): MenuFirstLinkFetcher {
    return $this->menuFirstLinkFetcher ??
      ($this->menuFirstLinkFetcher = \Drupal::service('sitetree.utility.menu_first_link_fetcher'));
  }

  protected function getAllItemsCacheability(): CacheableDependencyInterface {
    // Do not try to getFrontPagePath here, it will cause recursion.
    return (new CacheableMetadata())
      ->addCacheTags(['domain_list'])
      ->addCacheContexts(['url.site']);
  }

  protected function getSingleItemCacheability(string $name): ?CacheableDependencyInterface {
    if ($name === 'system.site') {
      $primaryMenuName = $this->fetchPrimaryMenuNameOfCurrentDomain();
      if ($primaryMenuName) {
        return MenuCacheability::forMenu($primaryMenuName);
      }
      return new CacheableMetadata();
    }
    return NULL;
  }

  protected function getOverrides(array $names): array {
    if (
      in_array('system.site', $names)
      && ($frontPagePath = $this->getFrontPagePath())
      && ($path = $frontPagePath->path)
    ) {
      return ['system.site' => ['page' => ['front' => $path]]];
    }
    else {
      return [];
    }
  }

  protected function getFrontPagePath(): FrontPagePathValue {
    $currentRequest = $this->requestStack->getCurrentRequest();
    if (!$this->frontPagePathPerRequest->offsetExists($currentRequest)) {
      $frontPagePath = $this->generateFrontPagePath();
      $this->frontPagePathPerRequest->offsetSet($currentRequest, $frontPagePath);
    }
    return $this->frontPagePathPerRequest->offsetGet($currentRequest);
  }

  protected function generateFrontPagePath(): FrontPagePathValue {
    $cacheabilityBuilder = (new CacheableMetadata())
      ->addCacheableDependency($this->getAllItemsCacheability());
    $path = NULL;
    $primaryMenuName = $this->fetchPrimaryMenuNameOfCurrentDomain();
    if ($primaryMenuName) {
      $firstLink = $this->getMenuFirstLinkFetcher()
        ->getFirstLink($primaryMenuName, $cacheabilityBuilder);
      $url = $firstLink->getUrl();
      if ($url->isRouted()) {
        $path = '/' . $url->getInternalPath();
      }
      $cacheabilityBuilder->addCacheableDependency(MenuCacheability::forMenu($primaryMenuName));
    }
    return new FrontPagePathValue($path, $cacheabilityBuilder);
  }

  public function fetchPrimaryMenuNameOfCurrentDomain(): ?string {
    $currentDomain = $this->getDomainNegotiator()->getActiveDomain();
    if ($currentDomain) {
      // No need to add domain cacheability, as we have domain_list tag.
      $menuNames = DomainSiteTreeTPS::getMenuNames($currentDomain);
      $primaryMenuName = reset($menuNames);
    }
    else {
      $primaryMenuName = NULL;
    }
    return $primaryMenuName;
  }


}
