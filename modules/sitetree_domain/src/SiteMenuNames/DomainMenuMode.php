<?php

declare(strict_types=1);

namespace Drupal\sitetree_domain\SiteMenuNames;

use Drupal\Core\StringTranslation\TranslatableMarkup;

enum DomainMenuMode: string {
  case None = 'none';
  case Active = 'active';
  case Canonical = 'canonical';
  case Exclusive = 'exclusive';

  public function label(): TranslatableMarkup {
    return match ($this) {
      self::None => t('None'),
      self::Active => t('Active'),
      self::Canonical => t('Canonical'),
      self::Exclusive => t('Exclusive'),
    };
  }

  public static function options(): array {
    // Sth like list comprehensions, nice.
    return iterator_to_array((function() {foreach(self::cases() as $case) yield $case->value => $case->label();})());
  }

}
