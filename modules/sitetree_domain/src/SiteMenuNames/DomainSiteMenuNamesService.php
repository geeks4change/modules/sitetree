<?php

declare(strict_types=1);

namespace Drupal\sitetree_domain\SiteMenuNames;

use Drupal\sitetree\SiteMenuNames\SiteMenuNamesBuilder;
use Drupal\sitetree_domain\DomainList\DomainListService;
use Drupal\sitetree_domain\DomainSiteTreeTPS;

final class DomainSiteMenuNamesService {

  public function __construct(
    protected DomainListService $domainListService,
  ) {}

  /**
   * @todo Cache this.
   */
  public function getDomainSiteMenuNames(): DomainSiteMenuNames {
    $all = new SiteMenuNamesBuilder();
    $canonical = new SiteMenuNamesBuilder();
    $exclusive = new SiteMenuNamesBuilder();

    $domainList = $this->domainListService->getDomainList();
    foreach ($domainList->allDomainsById as $domainId => $domain) {
      $all->add($domainId, ...DomainSiteTreeTPS::getMenuNames($domain));
      $canonical->add($domainId, ...DomainSiteTreeTPS::getCanonicalMenuNames($domain));
      $exclusive->add($domainId, ...DomainSiteTreeTPS::getExclusiveMenuNames($domain));
    }

    return new DomainSiteMenuNames(
      $domainList,
      $all->freeze(),
      $canonical->freeze(),
      $exclusive->freeze(),
    );
  }

}
