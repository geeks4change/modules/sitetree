<?php

declare(strict_types=1);

namespace Drupal\sitetree_domain\SiteMenuNames;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\sitetree\SiteMenuNames\SiteMenuNames;
use Drupal\sitetree_domain\DomainList\DomainList;

final class DomainSiteMenuNames {

  public function __construct(
    public readonly DomainList    $domains,
    public readonly SiteMenuNames $all,
    public readonly SiteMenuNames $canonical,
    public readonly SiteMenuNames $exclusive,
  ) {}

  public function getCacheability(): CacheableDependencyInterface {
    return (new CacheableMetadata())
      ->addCacheTags(['domain_list']);
  }

}
