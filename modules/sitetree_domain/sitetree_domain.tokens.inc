<?php

use Drupal\Core\Url;

/**
 * Implements hook_token_info().
 */
function sitetree_domain_token_info() {
  $info['types']['sitetree-domain'] = [
    'name' => t("SiteTree Domain"),
    'description' => t('Tokens for Domain SiteTree.'),
  ];

  $info['tokens']['sitetree-domain']['canonical-url'] = [
    'name' => t("Canonical URL"),
    'description' => t('The canonical URL.'),
  ];

  return $info;
}

/**
 * Implements hook_tokens().
 */
function sitetree_domain_tokens($type, $tokens, array $data, array $options, \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if ($type === 'sitetree-domain') {
    /** @var \Drupal\sitetree_domain\EffectiveUrlResult\EffectiveUrlResultAssembler $effectiveResultAssembler */
    $effectiveResultAssembler = \Drupal::service('sitetree_domain.effective_url_result_assembler');
    $url = Url::fromRouteMatch(\Drupal::routeMatch());
    foreach ($tokens as $name => $original) {
      if ($name === 'canonical-url') {
        $urlResult = $effectiveResultAssembler->getEffectiveResult($url);
        $bubbleable_metadata->addCacheableDependency($urlResult->getCacheability());
        $replacements[$original] = $urlResult->getCanonicalUrl();
      }
    }
  }
  return $replacements;
}