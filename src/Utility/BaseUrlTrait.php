<?php

namespace Drupal\sitetree\Utility;

use Drupal\Core\Url;

trait BaseUrlTrait {

  protected function getBaseUrl(): string {
    return rtrim(Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString(), '/');
  }

}
