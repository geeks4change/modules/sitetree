<?php

declare(strict_types=1);

namespace Drupal\sitetree\Utility;

use Drupal\domain\DomainInterface;

final class DomainTool {

  /**
   * Return domain base url (currently only host name, due to domain module).
   *
   * @see \Drupal\domain\Entity\Domain::buildUrl
   */
  public static function getBaseUrl(DomainInterface $domain): string {
    return $domain->getScheme() . $domain->getHostname();
  }

}
