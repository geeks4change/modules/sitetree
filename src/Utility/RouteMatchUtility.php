<?php

namespace Drupal\sitetree\Utility;

use Drupal\Core\ParamConverter\ParamConverterManagerInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Url;

/**
 * Route match utility.
 *
 * Taken from consistent_breadcrumbs and simplified.
 */
class RouteMatchUtility {

  protected ParamConverterManagerInterface $paramConverterManager;

  protected RouteProviderInterface $routeProvider;

  /**
   * Create a RouteMatch from an Url object.
   *
   * @param \Drupal\Core\Url $url
   *   The URL.
   *
   * @return \Drupal\Core\Routing\RouteMatchInterface
   *   The route match.
   */
  public function routeMatchFromUrl(Url $url): RouteMatchInterface {
    if (!$url->isRouted()) {
      throw new \RuntimeException('Only routed url can be casted to route match.');
    }
    else {
      $route = $this->routeProvider->getRouteByName($url->getRouteName());
      // @see \Drupal\Core\Access\AccessManager::checkNamedRoute
      $upcastedParameters = $this->paramConverterManager->convert($url->getRouteParameters() + $route->getDefaults());
      return new RouteMatch(
        $url->getRouteName(),
        $route,
        $upcastedParameters,
        $url->getRouteParameters()
      );
    }
  }

}
