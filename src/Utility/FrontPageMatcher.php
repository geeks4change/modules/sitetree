<?php

declare(strict_types=1);

namespace Drupal\sitetree\Utility;

use Drupal\CacheableTypes\CacheableBool;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

final class FrontPageMatcher {

  protected string $frontPagePath;

  protected CacheableDependencyInterface $frontPageCacheability;

  public function __construct(
    protected ConfigFactoryInterface $configFactory,
  ) {}

  /**
   * Match FrontPage for arbitrary URL.
   *
   * @see \Drupal\Core\Path\PathMatcher::isFrontPage
   */
  public function isFrontPage(Url $url): CacheableBool {
    $cacheability = (new CacheableMetadata())
      ->addCacheableDependency($url);
    $isFrontPage = FALSE;
    // Always add $frontPageCacheability, because a non routed URL can
    // become routed.
    $frontPagePath = $this->getFrontPagePath($cacheability);
    if ($url->isRouted()) {
      $isFrontPage = '/' . $url->getInternalPath() === $frontPagePath;
    }
    return CacheableBool::create($isFrontPage, $cacheability);
  }

  protected function getFrontPagePath(RefinableCacheableDependencyInterface $cacheabilityBuilder = NULL) {
    // Lazy-load front page config.
    if (!isset($this->frontPagePath)) {
      $siteConfig = $this->configFactory->get('system.site');
      $this->frontPagePath = $siteConfig
        ->get('page.front');
      $this->frontPageCacheability = $siteConfig;
    }
    // Because if clause.
    assert($this->frontPageCacheability);
    if ($cacheabilityBuilder) {
      $cacheabilityBuilder->addCacheableDependency($this->frontPageCacheability);
    }
    return $this->frontPagePath;
  }

  public function getFrontPageLink(RefinableCacheableDependencyInterface $cacheabilityBuilder = NULL): Link {
    return Link::fromTextAndUrl(t('Frontpage'), $this->getFrontPageUrl($cacheabilityBuilder));
  }

  public function getFrontPageUrl(RefinableCacheableDependencyInterface $cacheabilityBuilder = NULL): Url {
    return Url::fromUri('internal:/');
  }

}
