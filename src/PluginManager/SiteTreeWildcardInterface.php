<?php

namespace Drupal\sitetree\PluginManager;

use Drupal\CacheableTypes\CacheableBool;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Url;

/**
 * Interface for sitetree_wildcard plugins.
 */
interface SiteTreeWildcardInterface extends PluginFormInterface, ConfigurableInterface {

  public function matchUrl(Url $url): CacheableBool;

}
