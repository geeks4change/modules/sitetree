<?php

namespace Drupal\sitetree\PluginManager;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for sitetree_wildcard plugins.
 */
abstract class SiteTreeWildcardPluginBase extends PluginBase implements SiteTreeWildcardInterface, ContainerFactoryPluginInterface {

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }


  public function getConfiguration() {
    return [
        'id' => $this->getPluginId(),
      ] + $this->configuration;
  }

  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
    return $this;
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration = $form_state->getValues();
    foreach ($this->defaultConfiguration() as $key => $value) {
      $this->configuration[$key] = $form_state->getValue($key);
    }
  }

}
