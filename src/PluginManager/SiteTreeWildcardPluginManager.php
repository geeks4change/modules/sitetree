<?php

namespace Drupal\sitetree\PluginManager;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use function _PHPStan_e0e4f009c\RingCentral\Psr7\parse_query;

/**
 * SitetreeWildcard plugin manager.
 */
class SiteTreeWildcardPluginManager extends DefaultPluginManager {

  /**
   * Constructs SitetreeWildcardPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/SiteTreeWildcard',
      $namespaces,
      $module_handler,
      'Drupal\sitetree\PluginManager\SiteTreeWildcardInterface',
      'Drupal\sitetree\Annotation\SiteTreeWildcard'
    );
    $this->alterInfo('sitetree_wildcard_info');
    $this->setCacheBackend($cache_backend, 'sitetree_wildcard_plugins');
  }

  public function createInstanceFromQuery(array $configuration, CacheableDependencyInterface $cacheability): ?SiteTreeWildcardInterface {
    $plugin = $configuration['plugin'] ?? NULL;
    unset($configuration['plugin']);
    if (!is_string($plugin)) {
      return NULL;
    }
    $configuration['cacheability'] = $cacheability;

    try {
      /** @noinspection PhpUnhandledExceptionInspection */
      /** @noinspection PhpIncompatibleReturnTypeInspection */
      return $this->createInstance($plugin, $configuration);
    } catch (PluginNotFoundException $e) {
      return NULL;
    }
  }

  public static function toQuery(string $pluginId, array $configuration): string {
    unset($configuration['cacheability']);
    $configuration['plugin'] = $pluginId;
    return http_build_query($configuration);
  }

}
