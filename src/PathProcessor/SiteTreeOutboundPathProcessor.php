<?php

namespace Drupal\sitetree\PathProcessor;

use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\sitetree\SiteTree\SiteTreeMatcherInterface;
use Symfony\Component\HttpFoundation\Request;

final class SiteTreeOutboundPathProcessor implements OutboundPathProcessorInterface {

  public function __construct(
    protected SiteTreeMatcherInterface $siteTreeMultiMatcher,
  ) {}

  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    if (!$bubbleable_metadata) {
      $bubbleable_metadata = new BubbleableMetadata();
    }

    $baseUrl = NULL;
    foreach ($this->siteTreeMultiMatcher->getSiteTreeHandler() as $handler) {
      $handler->processOutbound($path, $options, $baseUrl, $bubbleable_metadata);
    }
    if ($baseUrl) {
      $options['base_url'] = $baseUrl;
      $options['absolute'] = TRUE;
    }
    return $path;
  }

}
