<?php

declare(strict_types=1);

namespace Drupal\sitetree;

use Drupal\sitetree\Breadcrumb\SiteTreeBreadcrumbAlterer;

/**
 * @internal
 */
final class SiteTreeServices {

  const WILDCARD_ROUTE = 'sitetree.wildcard';

  public static function getBreadcrumbAlterer(): SiteTreeBreadcrumbAlterer {
    return \Drupal::service('sitetree.breadcrumb_alterer');
  }

}
