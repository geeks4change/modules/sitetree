<?php

namespace Drupal\sitetree\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Link;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\sitetree\LinkTrail\MenuLinkTrail;
use Drupal\sitetree\SiteTree\SiteTreeMatcherInterface;
use Drupal\sitetree\Utility\FrontPageMatcher;

final class SiteTreeBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  public function __construct(
    protected SiteTreeMatcherInterface $siteTreeMultiMatcher,
    protected FrontPageMatcher         $frontPageMatcher,
  ) {}

  /**
   * @var array<\Drupal\Core\Breadcrumb\Breadcrumb>
   */
  protected array $builds = [];

  public function applies(RouteMatchInterface $route_match): bool {
    // Upstream: BreadcrumbBuilderInterface::applies lacks cacheability.
    // So add it in ::hookSystemBreadcrumbAlter
    return !!$this->getLinkTrail($route_match);
  }

  public function build(RouteMatchInterface $route_match): Breadcrumb {
    $linkTrail = $this->getLinkTrail($route_match);
    // Because ::applies().
    assert($linkTrail);
    // Add frontPage if it is not already last trail element.
    // Mind: Trail is root-last, contrary to breadcrumb.
    $isFrontPage = $this->frontPageMatcher->isFrontPage($linkTrail->last()->getUrlObject());
    $linkTrail = $linkTrail->withCacheability($isFrontPage);
    $frontUrlCacheabilityBuilder = new CacheableMetadata();
    $frontPageLink = $this->frontPageMatcher->getFrontPageLink($frontUrlCacheabilityBuilder);
    $frontPageToPrepend = $isFrontPage->value() ? NULL : $frontPageLink;
    return $linkTrail->toBreadcrumb($frontPageToPrepend);
  }

  protected function getLinkTrail(RouteMatchInterface $route_match): ?MenuLinkTrail {
    $handler = $this->siteTreeMultiMatcher->getSiteTreeHandler();
    // Because ::applies().
    assert($handler);
    $url = Url::fromRouteMatch($route_match);
    return $handler->getCompleteLinkTrail($url);
  }

}
