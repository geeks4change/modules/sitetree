<?php

declare(strict_types=1);

namespace Drupal\sitetree\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\sitetree\SiteTree\SiteTreeMatcherInterface;

final class SiteTreeBreadcrumbAlterer {

  public function __construct(
    protected SiteTreeMatcherInterface $siteTreeMultiMatcher,
  ) {}

  /**
   * If not a SiteTree breadcrumb, add non-match cacheability.
   *
   * @noinspection PhpParameterByRefIsNotUsedAsReferenceInspection*/
  public function hookSystemBreadcrumbAlter(Breadcrumb &$breadcrumb, RouteMatchInterface $routeMatch, array $context): void {
    if (!$context['builder'] instanceof SiteTreeBreadcrumbBuilder) {
      $handler = $this->siteTreeMultiMatcher->getSiteTreeHandler();
      $breadcrumb->addCacheableDependency($handler->getCacheability());
    }
  }

}
