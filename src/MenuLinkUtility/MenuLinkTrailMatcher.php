<?php

namespace Drupal\sitetree\MenuLinkUtility;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Url;
use Drupal\sitetree\LinkTrail\MenuLinkTrail;
use Drupal\sitetree\LinkTrail\MenuLinkTrailCollection;
use Drupal\sitetree\PluginManager\SiteTreeWildcardPluginManager;
use Drupal\sitetree\SiteTreeServices;

final class MenuLinkTrailMatcher {

  public function __construct(
    protected MenuLinkManagerInterface $menuLinkManager,
    protected SiteTreeWildcardPluginManager $wildcardPluginManager,
  ) {}

  /**
   * Maps any Url to its LinkTrails, used for access and breadcrumbs.
   *
   * No need to cache this, better cache EffectiveUrlResultAssembler result.
   *
   * @see \Drupal\Core\Menu\MenuActiveTrail::getActiveLink
   * @see \Drupal\Core\Menu\MenuActiveTrail::doGetActiveTrailIds
   */
  public function getAllMenuLinkTrails(Url $url): MenuLinkTrailCollection {
    $cacheability = (new CacheableMetadata())
      ->addCacheContexts(['route'])
      ->addCacheTags(['menu_list']);
    $collectionBuilder = MenuLinkTrailCollection::builder($cacheability);

    $matchingMenuLinks = $this->menuLinkManager->loadLinksByRoute($url->getRouteName(), $url->getRouteParameters());
    foreach ($matchingMenuLinks as $menuLink) {
      $collectionBuilder->addLinkTrail(MenuLinkTrail::create($menuLink, $cacheability));
    }

    $wildcardMenuLinks = $this->menuLinkManager->loadLinksByRoute(SiteTreeServices::WILDCARD_ROUTE);
    foreach ($wildcardMenuLinks as $menuLink) {
      $menuLinkQuery = $menuLink->getOptions()['query'] ?? [];
      $wildcardPlugin = $this->wildcardPluginManager
        ->createInstanceFromQuery($menuLinkQuery, $menuLink);
      if ($wildcardPlugin) {
        $matchResult = $wildcardPlugin->matchUrl($url);
        if ($matchResult->value()) {
          // @todo Adjust wildcard links to actual links.
          //   This currently only matters if wildcard is a parent.
          //   And then it needs a way for plugins to return parent urls.
          $collectionBuilder->addLinkTrail(MenuLinkTrail::create($menuLink, $cacheability));
        }
      }
    }
    return $collectionBuilder->freeze();
  }

}
