<?php

declare(strict_types=1);

namespace Drupal\sitetree\MenuLinkUtility;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Link;
use Drupal\Core\Menu\MenuLinkTreeElement;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeParameters;

final class MenuFirstLinkFetcher {

  public function __construct(
    protected MenuLinkTreeInterface $menuLinkTree,
  ) {}

  public function getFirstLink(string $menuName, RefinableCacheableDependencyInterface $cacheabilityBuilder): ?Link {
    $link = NULL;
    $parameters = (new MenuTreeParameters())->setMaxDepth(1);
    $rootMenuLinks = $this->menuLinkTree->load($menuName, $parameters);
    if ($rootMenuLinks) {
      // Sort by weight.
      // @see \Drupal\Core\Menu\MenuParentFormSelector::getParentSelectOptions
      $manipulators = [
        ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
      ];
      $sortedRootMenuLinks = $this->menuLinkTree->transform($rootMenuLinks, $manipulators);
      /** @var MenuLinkTreeElement $firstRootMenuLinkTreeElement */
      $firstRootMenuLinkTreeElement = reset($sortedRootMenuLinks);
      $link = Link::fromTextAndUrl(
        $firstRootMenuLinkTreeElement->link->getTitle(),
        $firstRootMenuLinkTreeElement->link->getUrlObject()
      );
      // Add menu cacheability. This is also invalidated if menu links change.
      $cacheabilityBuilder->addCacheableDependency(MenuCacheability::forMenu($menuName));
    }
    return $link;
  }

}
