<?php

declare(strict_types=1);

namespace Drupal\sitetree\MenuLinkUtility;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;

final class MenuCacheability {

  /**
   * Get menu items list cacheability.
   *
   * @see \Drupal\system\Plugin\Block\SystemMenuBlock::getCacheTags
   * @see \Drupal\Core\Menu\MenuTreeStorage::delete
   */
  public static function forMenu(string $menuName): CacheableDependencyInterface {
    $prefix = self::getPrefix();
    return (new CacheableMetadata())
      ->addCacheTags(["$prefix$menuName"]);
  }

  public static function getPrefix(): string {
    return "config:system.menu.";
  }

}
