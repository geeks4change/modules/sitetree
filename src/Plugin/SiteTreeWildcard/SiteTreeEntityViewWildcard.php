<?php

namespace Drupal\sitetree\Plugin\SiteTreeWildcard;

use Drupal\CacheableTypes\CacheableBool;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\sitetree\PluginManager\SiteTreeWildcardPluginBase;
use Drupal\views\Plugin\views\query\Sql;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the sitetree_wildcard.
 *
 * @SiteTreeWildcard(
 *   id = "entity_view",
 *   label = @Translation("Entity View"),
 *   description = @Translation("Use view as entity wildcard."),
 * )
 */
class SiteTreeEntityViewWildcard extends SiteTreeWildcardPluginBase {

  use StringTranslationTrait;

  protected EntityTypeManagerInterface $entityTypeManager;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  public function defaultConfiguration() {
    return [
      'view' => NULL,
      'display' => NULL,
      // @todo Add view arguments.
      // More in settings (but not here due to ::submitConfigurationForm()):
      // - cacheability
    ];
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['view'] = [
      '#title' => $this->t('View'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $this->getConfiguration()['view'],
    ];
    $form['display'] = [
      '#title' => $this->t('Display'),
      '#type' => 'textfield',
      '#required' => FALSE,
      '#default_value' => $this->getConfiguration()['display'],
    ];
    return $form;
  }

  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // @todo Add form validation.
  }

  public function matchUrl(Url $url): CacheableBool {
    $entity = $this->extractEntity($url);
    if (!$entity) {
      return CacheableBool::create(FALSE, $this->getCacheability());
    }

    $view = $this->getInitializedViewDisplay();
    if (!$view) {
      return CacheableBool::create(FALSE, $this->getCacheability());
    }

    $isViewForEntityType = $this->isViewForEntityType($view, $entity->getEntityType());
    if (!$isViewForEntityType) {
      return CacheableBool::create(FALSE, $this->getCacheability());
    }

    // Build the query. Looks like no preExecute is needed.
    $view->build();
    $query = $view->getQuery();
    if (!$query instanceof Sql) {
      return CacheableBool::create(FALSE, $this->getCacheability());
    }

    // Add ID condition, set limit, build again.
    // Good for us that the main group always is "AND".
    // @see \Drupal\views\Plugin\views\query\Sql::buildCondition
    $idKey = $entity->getEntityType()->getKey('id');
    $query->addWhere('sitetree', $idKey, $entity->id());
    $query->setLimit(1);
    $query->build($view);

    $view->execute();
    $matches = count($view->result) > 0;
    return CacheableBool::create($matches, $this->getCacheability());
  }

  protected function getInitializedViewDisplay(): ?ViewExecutable {
    $view = Views::getView($this->getConfiguration()['view']);
    // This also initializes the display plugin.
    if (!$view || !$view->access($this->getConfiguration()['display'] ?? 'default')) {
      return NULL;
    }
    return $view;
  }

  protected function getCacheability(): CacheableDependencyInterface {
    return $this->getConfiguration()['cacheability'] ?? throw new \UnexpectedValueException('Need cacheability.');
  }

  protected function extractEntity(Url $url): ?EntityInterface {
    if ($url->isRouted()) {
      if (preg_match('~^entity\\.(.*)\\.canonical$~', $url->getRouteName(), $matches)) {
        $entityTypeId = $matches[1];
        $entityType = $this->entityTypeManager->getDefinition($entityTypeId, FALSE);
        if ($entityType) {
          $entityId = $url->getRouteParameters()[$entityTypeId] ?? NULL;
          $storage = $this->entityTypeManager->getStorage($entityTypeId);
          return $storage->load($entityId);
        }
      }
    }
    return NULL;
  }

  protected function isViewForEntityType(ViewExecutable $view, EntityTypeInterface $entityType): bool {
    return $view->getBaseEntityType()->id() === $entityType->id();
  }

}
