<?php

declare(strict_types=1);

namespace Drupal\sitetree\SiteMenuNames;

final class SiteMenuNames {

  /**
   * @param string[][] $menuNamesBySite
   */
  public function __construct(
    protected array $menuNamesBySite,
  ) {}

  public static function builder(): SiteMenuNamesBuilder {
    return new SiteMenuNamesBuilder();
  }

  /**
   * @return string[]
   */
  public function getMenuNames(string $site): array {
    return $this->menuNamesBySite[$site] ?? throw new \UnexpectedValueException();
  }

  public function getSitesHavingMenuNames(array $menuNames): array {
    $siteIds = [];
    foreach ($this->menuNamesBySite as $siteId => $siteMenuNames) {
      if (array_intersect($menuNames, $siteMenuNames)) {
        $siteIds[] = $siteId;
      }
    }
    return $siteIds;
  }

  public function getFirstSiteHavingMenuNames(array $menuNames): ?string {
    $siteIds = $this->getSitesHavingMenuNames($menuNames);
    return reset($siteIds) ?: NULL;
  }

}
