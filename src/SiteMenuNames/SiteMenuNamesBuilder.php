<?php

declare(strict_types=1);

namespace Drupal\sitetree\SiteMenuNames;

final class SiteMenuNamesBuilder {

  protected array $menuNamesBySite = [];

  public function __construct() {}

  public function add(string $site, string ...$menuNames) {
    $this->menuNamesBySite[$site] = $menuNames;
  }

  public function freeze(): SiteMenuNames {
    return new SiteMenuNames($this->menuNamesBySite);
  }

}
