<?php

namespace Drupal\sitetree\SiteTree;

interface SiteTreeMultiMatcherInterface extends SiteTreeMatcherInterface {

  public function addMatcher(SiteTreeMatcherInterface $handler);

}
