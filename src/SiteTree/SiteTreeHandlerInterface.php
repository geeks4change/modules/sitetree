<?php

namespace Drupal\sitetree\SiteTree;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\sitetree\LinkTrail\MenuLinkTrail;
use Drupal\sitetree_domain\ConfigOverride\FrontPagePathValue;

interface SiteTreeHandlerInterface {

  public function getAccess(Url $url, AccountInterface $account): AccessResultInterface;

  public function processOutbound(string &$path, array &$options, ?string &$baseUrl, BubbleableMetadata $bubbleable_metadata): void;

  /**
   * Get maybe LinkTrail for a URL.
   *
   * @return ?MenuLinkTrail
   *   The menu link trail for the URL, if applicable.
   *   First link MUST be link for the URL.
   *   Last link MUST be frontpage (maybe from another menu).
   *   Links between MUST be menu ancestors of first link.
   */
  public function getCompleteLinkTrail(Url $url): ?MenuLinkTrail;

  public function getCacheability(): CacheableDependencyInterface;

}
