<?php

namespace Drupal\sitetree\SiteTree;

use Drupal\Core\Url;

interface SiteTreeMatcherInterface {

  public function getSiteTreeHandler(): SiteTreeHandlerInterface;

}
