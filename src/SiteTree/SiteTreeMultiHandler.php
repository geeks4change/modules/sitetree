<?php

declare(strict_types=1);

namespace Drupal\sitetree\SiteTree;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\sitetree\LinkTrail\MenuLinkTrail;

final class SiteTreeMultiHandler implements SiteTreeHandlerInterface {

  /**
   * @param \Drupal\sitetree\SiteTree\SiteTreeHandlerInterface[] $handlers
   */
  private function __construct(
    protected array $handlers,
  ) {}

  /**
   * @param \Drupal\sitetree\SiteTree\SiteTreeHandlerInterface[] $handlers
   */
  public static function create(array $handlers): static {
    return new static($handlers);
  }

  public function getAccess(Url $url, AccountInterface $account): AccessResultInterface {
    $accessResult = AccessResult::allowed();
    foreach ($this->handlers as $handler) {
      $accessResult = $accessResult->andIf($handler->getAccess($url, $account));
    }
    return $accessResult;
  }

  public function processOutbound(string &$path, array &$options, ?string &$baseUrl, BubbleableMetadata $bubbleable_metadata): void {
    foreach ($this->handlers as $handler) {
      $handler->processOutbound($path, $options, $baseUrl, $bubbleable_metadata);
    }
  }

  public function getCompleteLinkTrail(Url $url): ?MenuLinkTrail {
    $linkTrail = NULL;
    foreach ($this->handlers as $handler) {
      $newLinkTrail = $handler->getCompleteLinkTrail($url);
      $linkTrail = !$linkTrail ? $newLinkTrail : $linkTrail->merge($newLinkTrail);
    }
    return $linkTrail;
  }

  public function getCacheability(): CacheableDependencyInterface {
    $cacheability = new CacheableMetadata();
    foreach ($this->handlers as $handler) {
      $cacheability->addCacheableDependency($handler->getCacheability());
    }
    return $cacheability;
  }

}
