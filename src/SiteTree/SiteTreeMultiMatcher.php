<?php

declare(strict_types=1);

namespace Drupal\sitetree\SiteTree;

final class SiteTreeMultiMatcher implements SiteTreeMultiMatcherInterface {

  /**
   * @var \Drupal\sitetree\SiteTree\SiteTreeMatcherInterface[][]
   */
  protected array $collectMatchersByPriority = [];

  /**
   * @var \Drupal\sitetree\SiteTree\SiteTreeMatcherInterface[]
   */
  protected array $matchers;

  public function addMatcher(SiteTreeMatcherInterface $handler, int $priority = 0): void {
    if (isset($this->matchers)) {
      throw new \LogicException('Can not add after sorting.');
    }
    $this->collectMatchersByPriority[$priority][] = $handler;
  }

  /**
   * @return \Drupal\sitetree\SiteTree\SiteTreeMatcherInterface[]
   */
  protected function getMatchers(): array {
    if (!isset($this->matchers)) {
      krsort($this->collectMatchersByPriority);
      $this->matchers = array_merge([], ...$this->collectMatchersByPriority);
    }
    return $this->matchers;
  }

  public function getSiteTreeHandler(): SiteTreeHandlerInterface {
    return SiteTreeMultiHandler::create(array_map(
      fn(SiteTreeMatcherInterface $matcher) => $matcher->getSiteTreeHandler(),
      $this->getMatchers()
    ));
  }

}
