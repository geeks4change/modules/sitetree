<?php

namespace Drupal\sitetree\Access;

use Drupal\Core\Access\AccessCheckInterface;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\sitetree\SiteTree\SiteTreeMatcherInterface;
use Symfony\Component\Routing\Route;

class SiteTreeRouteAccess implements AccessCheckInterface {

  public function __construct(
    protected SiteTreeMatcherInterface $siteTreeMultiMatcher
  ) {}

  public function applies(Route $route): bool {
    return TRUE;
  }

  /**
   * @see \Drupal\domain\Access\DomainAccessCheck::access
   */
  public function access(RouteMatchInterface $routeMatch, AccountInterface $account): AccessResultInterface {
    $url = Url::fromRouteMatch($routeMatch);
    return $this->siteTreeMultiMatcher->getSiteTreeHandler()->getAccess($url, $account);
  }

}
