<?php

declare(strict_types=1);

namespace Drupal\sitetree\LinkTrail;

use Drupal\Core\Cache\CacheableDependencyInterface;

final class MenuLinkTrailMap {

  /**
   * @param \Drupal\sitetree\LinkTrail\MenuLinkTrail[] $menuLinkTrailsBySite
   *
   * @internal Use ::builder().
   */
  public function __construct(
    protected array                        $menuLinkTrailsBySite,
    protected CacheableDependencyInterface $cacheability,
  ) {}

  public function getMenuLinkTrail(string $site): ?MenuLinkTrail {
    return $this->menuLinkTrailsBySite[$site] ?? NULL;
  }

  public function getCacheability(): CacheableDependencyInterface {
    return $this->cacheability;
  }

}
