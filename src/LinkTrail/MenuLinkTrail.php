<?php

declare(strict_types=1);

namespace Drupal\sitetree\LinkTrail;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Link;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\sitetree\MenuLinkUtility\MenuCacheability;

final class MenuLinkTrail {

  protected array $menuLinks;

  protected CacheableDependencyInterface $cacheability;

  protected function __construct(
    protected MenuLinkInterface            $firstMenuLink,
    CacheableDependencyInterface $additionalCacheability,
  ) {
    $this->cacheability = (new CacheableMetadata())
      ->addCacheableDependency($additionalCacheability)
      ->addCacheableDependency(
        MenuCacheability::forMenu($this->firstMenuLink->getMenuName())
      );
  }

  public static function create(
    MenuLinkInterface            $firstMenuLink,
    CacheableDependencyInterface $additionalCacheability,
  ): static {
    return new static($firstMenuLink, $additionalCacheability);
  }

  protected function lazyLoadMenuLinks(): void {
    if (!isset($this->menuLinks)) {
      /** @var \Drupal\Core\Menu\MenuLinkManagerInterface $menuLinkManager */
      $menuLinkManager = \Drupal::service('plugin.manager.menu.link');
      $linkAndAncestorIds = $menuLinkManager->getParentIds($this->firstMenuLink->getPluginId());
      // No result if no ancestors OR link invalid.
      if ($linkAndAncestorIds) {
        foreach ($linkAndAncestorIds as $ancestorId) {
          /** @noinspection PhpUnhandledExceptionInspection */
          $menuLink = $menuLinkManager->createInstance($ancestorId);
          assert($menuLink instanceof MenuLinkInterface);
          $this->menuLinks[] = $menuLink;
        }
      }
    }
  }
  public function getMenuName(): string {
    return $this->firstMenuLink->getMenuName();
  }

  public function last(): MenuLinkInterface {
    $this->lazyLoadMenuLinks();
    $first = end($this->menuLinks);
    // Exists because constructor.
    assert($first instanceof MenuLinkInterface);
    return $first;
  }

  public function getCacheability(): CacheableDependencyInterface {
    return $this->cacheability;
  }

  public function withCacheability(CacheableDependencyInterface $cacheability) {
    $newCacheability1 = (new CacheableMetadata())
      ->addCacheableDependency($this->cacheability)
      ->addCacheableDependency($cacheability)
    ;
    return new static($this->firstMenuLink, $newCacheability1);
  }

  public function toBreadcrumb(?Link $frontPageLinkToPrepend = NULL): Breadcrumb {
    $this->lazyLoadMenuLinks();
    $links = array_reverse(array_map(
      $this->toLink(...),
      $this->menuLinks
    ));
    // Last link is current page, drop it.
    array_pop($links);

    if ($frontPageLinkToPrepend) {
      array_unshift($links, $frontPageLinkToPrepend);
    }
    return (new Breadcrumb())
      ->addCacheableDependency($this->cacheability)
      ->setLinks($links);
  }

  protected function toLink(MenuLinkInterface $menuLink): Link{
    return Link::fromTextAndUrl($menuLink->getTitle(), $menuLink->getUrlObject());
  }

}
