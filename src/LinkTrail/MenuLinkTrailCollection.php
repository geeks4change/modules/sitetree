<?php

declare(strict_types=1);

namespace Drupal\sitetree\LinkTrail;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;

final class MenuLinkTrailCollection {

  /**
   * @param \Drupal\sitetree\LinkTrail\MenuLinkTrail[] $menuLinkTrails
   * @internal Use ::builder().
   */
  public function __construct(
    protected array                        $menuLinkTrails,
    protected CacheableDependencyInterface $cacheability,
  ) {}

  public static function builder(CacheableMetadata $cacheability): MenuLinkTrailCollectionBuilder {
    return new MenuLinkTrailCollectionBuilder($cacheability);
  }

  public function getMenuNames() {
    return array_unique(array_map(
      fn(MenuLinkTrail $menuLinkTrail) => $menuLinkTrail->getMenuName(),
      $this->menuLinkTrails
    ));
  }

  public function first(): ?MenuLinkTrail {
    return reset($this->menuLinkTrails) ?: NULL;
  }

  public function getFirstMenuLinkTrailForMenus(string ...$menuNames): ?MenuLinkTrail {
    // Prioritize earlier menus before later ones.
    foreach ($menuNames as $menuName) {
      foreach ($this->menuLinkTrails as $menuLinkTrail) {
        if ($menuLinkTrail->getMenuName() === $menuName) {
          return $menuLinkTrail;
        }
      }
    }
    return NULL;
  }

  public function getCacheability(): CacheableDependencyInterface {
    return $this->cacheability;
  }

}
