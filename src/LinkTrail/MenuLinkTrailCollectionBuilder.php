<?php

declare(strict_types=1);

namespace Drupal\sitetree\LinkTrail;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;

final class MenuLinkTrailCollectionBuilder {

  protected array $linkTrails = [];

  protected RefinableCacheableDependencyInterface $cacheabilityBuilder;

  public function __construct(
    CacheableDependencyInterface $cacheability,
  ) {
    $this->cacheabilityBuilder = (new CacheableMetadata())
      ->addCacheableDependency($cacheability);
  }

  public function addLinkTrail(MenuLinkTrail $linkTrail): static {
    $this->linkTrails[] = $linkTrail;
    $this->addCacheableDependency($linkTrail->getCacheability());
    return $this;
  }

  public function addCacheableDependency(CacheableDependencyInterface $cacheability): static {
    $this->cacheabilityBuilder->addCacheableDependency($cacheability);
    return $this;
  }

  public function freeze(): MenuLinkTrailCollection {
    return new MenuLinkTrailCollection($this->linkTrails, $this->cacheabilityBuilder);
  }

}
