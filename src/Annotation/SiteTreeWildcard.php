<?php

namespace Drupal\sitetree\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines sitetree_wildcard annotation object.
 *
 * @Annotation
 */
class SiteTreeWildcard extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
