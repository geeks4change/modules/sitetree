# SiteTree
## Principles
- Sites implement the idea of sub-universes of a main site.
- Sites may be ordinary (not affecting the main site) or exclusive (taking part of its content out of the main site).
- Sites are always defined by menus, and the menu hierarchy is reflected in breadcrumbs.
- As that has its limitations for e.g. a list of articles, sitetree implements wildcard menu entries via magic URLs.
- With SiteTreeDomains module, sites are domains
- In the future, groups may define sites, either as sub-site *in the group*, or on an own domain, likely via groupcontentmenu module.
- Also in the future, embeddings may be defined as sub-site.
- Note: Using group as access layer for domain content should be possible today though.

## Setting up canonical URL
- Add and enable the *Metatag* module.
- In the *Global* configuration, under *Canonical URL*, enter the token *[sitetree-domain:canonical-url]*.

## The rules
- Every domain can be associated with multiple menus
- Every menu can be defined *active*, *canonical*, or *exclusive*.
- One of these menus must be defined as the primary menu.
- Menu items can be ordinary links, or wildcard links.
  - (Currently) For the access check via menu matching, does not matter if a link is *active* or not (we need this for wildcards).
  - Ordinary (internal) links match its URL.
  - Wildcard links match multiple internal URLs.
  - Example: `/sitetree-wildcard?plugin=entity_view&view=articles&display=block` matches all entities of that view's display.
  - A domain match is called *canonical* or *exclusive*, if the menu of its link is marked so on that domain.
  - Note that a menu (think: footer menu) may be associated to multiple domains.
### Access
- Basically:
  - Every URL has access on the default domain.
  - URLs are permitted to other domains' by its configurable menus. I.e., a URL is accessible on one domain, if it is on one or more of its menus (either explicitly or by wildcard).
- Except if one menu containing a URL match is marked *exclusive*:
  - Then the URL is accessible only on domains with such *exclusive* matches (and not on others, and not on default domain).
### Outbound links
- If a link on a page HAS access on the current domain, it links there, i.e. to the URL on the current domain.
- If the link does NOT have access on the current domain,
  - It links to the first *canonical* match, if exists.
  - Otherwise, it links to the first domain with access.
  - Otherwise, it links to the default domain (if exists).
### Canonical link
- Every page receives a *canonical* link (maybe to itself, without query parameters).
- It links to the first *canonical* match, if exists.
- Otherwise to the domain itself, if access.
- (If no access, redirection kicks in, and this is not an issue.)
### Redirection
- Every page with access denied by us get a redirection.
- The redirection target is that described in *Outbound links*.
### Breadcrumbs
- Every page with access granted by a match gets the breadcrumbs of that match, plus the front page if not already in it.
### Frontpage
- The front page of every domain is the first root link of its primary menu.

## Limitations of SiteTreeDomains
- Does NOT support site subdirectories like http://my.domain/drupal.
- Does not support domain aliases.

