<?php

namespace Drupal\Tests\sitetree\Traits;

use Drupal\domain\DomainInterface;
use Drupal\domain\Entity\Domain;

trait CreateMyDomainsTrait {

  protected function createTestDomains(int $count) {
    $entityStorage = \Drupal::entityTypeManager()->getStorage('domain');
    $host = \Drupal::request()->getHttpHost();
    foreach (range(0, $count - 1) as $i) {
      $prefix = $this->getDomainPrefix($i);
      $values = [
        'hostname' => "$prefix.$host",
        'name' => 'Domain ' . ucfirst($prefix),
        'id' => $this->getDomainId($i),
      ];
      $domain = $entityStorage->create($values);
      $domain->save();
    }
  }

  protected function loadTestDomain(int $i): DomainInterface {
    return Domain::load($this->getDomainId($i));
  }

  protected function getDomainUrl(int $i): string {
    return 'http://' . $this->loadTestDomain($i)->getHostname();
  }

  private function getDomainId(int $i): string {
    return "{$i}_example_com";
  }

  private function getDomainPrefix(int $i) {
    return ['zero', 'one', 'two', 'three', 'four', 'five'][$i] ?? throw new \UnexpectedValueException();
  }

}
