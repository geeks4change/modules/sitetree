<?php

namespace Drupal\Tests\sitetree\Traits;

use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\node\Entity\Node;
use Drupal\system\Entity\Menu;

trait CreateMyEntitiesTrait {

  protected function assertEntityCanSave(string $entityClass, array $values) {
    /** @noinspection PhpUndefinedMethodInspection */
    $entity = $entityClass::create($values);
    $success = $entity->save();
    if (!$success) {
      throw new \RuntimeException('Can not save entity: ' . var_export($values, 1));
    }
    return $entity;
  }

  protected function createNode(array $values = []) {
    return $this->assertEntityCanSave(Node::class, $values);
  }

  protected function createMenu(array $values) {
    return $this->assertEntityCanSave(Menu::class, $values);
  }

  protected function createMenuLinkContent(array $values) {
    return $this->assertEntityCanSave(MenuLinkContent::class, $values);
  }

}
