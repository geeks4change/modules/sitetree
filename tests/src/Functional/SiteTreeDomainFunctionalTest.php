<?php

declare(strict_types=1);
namespace Drupal\Tests\sitetree\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\sitetree\Traits\CreateMyDomainsTrait;
use Drupal\Tests\sitetree\Traits\CreateMyEntitiesTrait;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Test SiteTree.
 *
 * @group sitetree
 */
final class SiteTreeDomainFunctionalTest extends BrowserTestBase {

  use CreateMyEntitiesTrait;
  use CreateMyDomainsTrait;

  /**
   * @var \Drupal\node\NodeInterface[]
   */
  protected array $nodes = [];

  /**
   * @var \Drupal\menu_link_content\Entity\MenuLinkContent[]
   */
  protected array $links = [];

  protected static $modules = [
    'sitetree_test',
  ];

  protected $defaultTheme = 'stark';

  protected function setUp(): void {
    parent::setUp();

    // Prepare VarDumper, just in case we need it.
    VarDumper::setHandler(function ($v) {
      $cloner = new VarCloner();
      $dumper = new CliDumper();
      $output = fopen('dump.txt', 'a');
      $dumper->dump($cloner->cloneVar($v), $output);
    });


    // Create domains zero, one, and two.
    $this->createTestDomains(3);

    // Add out canonical url tag.
    \Drupal::configFactory()->getEditable('metatag.metatag_defaults.node')
      ->set('tags.canonical_url', '[sitetree-domain:canonical-url]')
      ->save();


    // Create Page0 as frontPage of domain zero.
    $this->nodes['p0'] = $this->createNode([
      'type' => 'tiny_page',
      'title' => 'Page0',
    ]);

    $this->config('system.site')
      ->set('page.front', $this->nodes['p0']->toUrl()->toString())
      ->save();
    \Drupal::service('router.builder')->rebuild();


    // Create domain one, page1/1a , articles and view, menu one (canonical).
    $this->createMenu([
      'id' => 'one',
      'label' => 'One',
    ]);
    $this->loadTestDomain(1)
      ->setThirdPartySetting('sitetree_domain', 'menus', ['one' => 'canonical'])
      ->save();

    $this->nodes['p1'] = $this->createNode([
      'type' => 'tiny_page',
      'title' => 'Page1',
    ]);
    $this->links['p1'] = $this->createMenuLinkContent([
      'menu_name' => 'one',
      'title' => 'P1',
      'weight' => 0,
      'parent' => '',
      'link' => ['title' => 'Link1', 'uri' => $this->nodes['p1']->toUrl()->toUriString()],
    ]);


    $this->nodes['p1a'] = $this->createNode([
      'type' => 'tiny_page',
      'title' => 'Page1a',
    ]);
    $this->links['p1a'] = $this->createMenuLinkContent([
      'menu_name' => 'one',
      'title' => 'P1a',
      'weight' => 1,
      'parent' => 'menu_link_content:' . $this->links['p1']->uuid(),
      'link' => ['title' => 'Link1', 'uri' => $this->nodes['p1a']->toUrl()->toUriString()],
    ]);

    $this->nodes['p1b'] = $this->createNode([
      'type' => 'tiny_page',
      'title' => 'Page1b',
    ]);
    $this->links['p1b'] = $this->createMenuLinkContent([
      'menu_name' => 'one',
      'title' => 'P1b',
      'weight' => 0,
      'parent' => 'menu_link_content:' . $this->links['p1a']->uuid(),
      'link' => ['title' => 'Link1', 'uri' => $this->nodes['p1b']->toUrl()->toUriString()],
    ]);

    $this->nodes['a1'] = $this->createNode([
      'type' => 'tiny_article',
      'title' => 'Article1',
    ]);
    $this->nodes['a2'] = $this->createNode([
      'type' => 'tiny_article',
      'title' => 'Article2',
    ]);

    $this->links['articles'] = $this->createMenuLinkContent([
      'menu_name' => 'one',
      'title' => 'Articles',
      'weight' => 0,
      'parent' => 'menu_link_content:' . $this->links['p1']->uuid(),
      'link' => ['title' => 'Link1', 'uri' => 'internal:/articles'],
    ]);

    $this->links['article-wildcard'] = $this->createMenuLinkContent([
      'menu_name' => 'one',
      'title' => 'Article',
      'weight' => 0,
      'parent' => 'menu_link_content:' . $this->links['articles']->uuid(),
      'link' => ['title' => 'Link1', 'uri' => 'internal:/sitetree-wildcard?plugin=entity_view&view=articles'],
    ]);


    // Create page and menu 2 (exclusive).
    $this->createMenu([
      'id' => 'two',
      'label' => 'Two',
    ]);
    $this->loadTestDomain(2)
      ->setThirdPartySetting('sitetree_domain', 'menus', ['two' => 'exclusive'])
      ->save();
    $this->nodes['p2'] = $this->createNode([
      'type' => 'tiny_page',
      'title' => 'Page2',
    ]);
    $this->links['p2'] = $this->createMenuLinkContent([
      'menu_name' => 'two',
      'title' => 'P2',
      'weight' => 0,
      'link' => ['title' => 'Link2', 'uri' => $this->nodes['p2']->toUrl()->toUriString()],
    ]);

    \Drupal::service('router.builder')->rebuild();

  }

  public function getUrlStringFor(int $domainNumber, ?string $nodeName = NULL): string {
    $urlString = $this->getDomainUrl($domainNumber);
    if ($nodeName) {
      $urlString .= $this->nodes[$nodeName]->toUrl()->toString();
    }
    return $urlString;
  }

  /**
   * Get url of Node on Domain. If Node missing, then frontpage.
   */
  private function getDomainNodeUrl(int $domainNumber, string $nodeName = NULL): void {
    $urlString = $this->getUrlStringFor($domainNumber, $nodeName);
    $this->drupalGet($urlString);
  }


  protected function assertBreadcrumbLabels(string ...$expectedLabels) {
    $nodes = $this->cssSelect('nav h2#system-breadcrumb ~ ol li a');
    $actualLabels = array_map(fn($node) => $node->getText(), $nodes);
    $this->assertSame($expectedLabels, $actualLabels);
  }

  protected function assertCanonicalLink(string $url) {
    $xpath = '//head/link[@rel="canonical"]/@href';
    $canonicalLinks = $this->xpath($xpath);
    $this->assertSame(1, count($canonicalLinks));
    $this->assertSame($url, $canonicalLinks[0]->getText());
  }



  public function testFrontPageOnDomainZero() {
    $this->getDomainNodeUrl(0);
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->pageTextContains('Page0');
  }

  public function testFrontPageOnDomainOne() {
    $this->getDomainNodeUrl(1);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Page1');
  }

  public function testPageOneOnDomainZeroAlso() {
    $this->getDomainNodeUrl(0, 'p1');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Page1');
  }

  public function testPageOneOnDomainTwoRedirectsToDomainOne() {
    $this->getDomainNodeUrl(2, 'p1');
    $this->assertSame($this->getUrlStringFor(1, 'p1'), $this->getSession()->getCurrentUrl());
  }

  public function testPageZeroOnDomainOneRedirectsToDomainZero() {
    $this->getDomainNodeUrl(1, 'p0');
    $this->assertSame($this->getUrlStringFor(0, 'p0'), $this->getSession()->getCurrentUrl());
  }

  public function testPageZeroOnDomainOTwoRedirectsToDomainZero() {
    $this->getDomainNodeUrl(2, 'p0');
    $this->assertSame($this->getUrlStringFor(0, 'p0'), $this->getSession()->getCurrentUrl());
  }

  public function testPageTwoOnDomainZeroRedirectsToDomainTwo() {
    $this->getDomainNodeUrl(0, 'p2');
    $this->assertSame($this->getUrlStringFor(2, 'p2'), $this->getSession()->getCurrentUrl());
  }

  public function testPageTwoOnDomainOneRedirectsToDomainTwo() {
    $this->getDomainNodeUrl(1, 'p2');
    $this->assertSame($this->getUrlStringFor(2, 'p2'), $this->getSession()->getCurrentUrl());
  }

  public function testArticleTwoOnDomainOneHasBreadcrumb() {
    $this->getDomainNodeUrl(1, 'a2');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->titleEquals('Article2 | Drupal');
    $this->assertBreadcrumbLabels('P1', 'Articles');
  }

  public function testPageOneOnDomainOneHasBreadcrumb() {
    $this->getDomainNodeUrl(1, 'p1b');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->titleEquals('Page1b | Drupal');
    $this->assertBreadcrumbLabels('P1', 'P1a');
  }

  public function testUrlWithoutRouteDoesNotThrow() {
    $urlString = $this->getDomainUrl(0) . '/favicon.ico';
    $this->drupalGet($urlString);
    $this->assertSession()->statusCodeEquals(404);
  }

  public function testArticleTwoOnDomainZeroHasCanonicalDomainOne() {
    $this->getDomainNodeUrl(0, 'a2');
    $this->assertSession()->statusCodeEquals(200);
    // Metatag does NOT add a http header. @link https://www.drupal.org/project/metatag/issues/3241256
    // $this->assertContains("<$url>; rel=\"canonical\"", $response->headers->all('link'));
    $url = $this->getUrlStringFor(1, 'a2');
    $this->assertCanonicalLink($url);
  }

  public function testFrontPageOnDomainTwoHasItselfAsCanonicalLink() {
    $this->getDomainNodeUrl(2);
    $this->assertCanonicalLink($this->getUrlStringFor(2) . '/');
  }

}
